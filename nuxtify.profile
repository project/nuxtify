<?php

/**
 * @file
 * Overrrides for the Nuxtify installation profile.
 */

/**
 * Implements hook_install_tasks().
 *
 * Trick Gin into thinking that the admin theme is Claro.
 * This fixes an issue where Gin expects that User entities already exist.
 */
function nuxtify_install_tasks() {
  global $config;

  $config['system.theme']['admin'] = 'claro';
  $config['system.theme']['default'] = 'claro';
}

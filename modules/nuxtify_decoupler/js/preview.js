Drupal.behaviors.nuxtifyDecouplerPreviewBehaviour = {
  attach: function (context) {
    once(
      'nuxtifyDecouplerPreviewBehaviour',
      'iframe.iframe--autoheight',
      context,
    ).forEach(
      (element) => {
        window.addEventListener('message', (e) => {
          const { data } = e

          if (data?.name === 'setFrameHeight') {
            element.style.minHeight = `${data.height}px`
          }
        })

        const debounce = (func, wait = 20) => {
          let timer

          return function (...args) {
            if (timer)
              clearTimeout(timer)

            timer = setTimeout(() => {
              func.apply(this, args)
            }, wait)
          }
        }


        const resizeToWindowHeight = (element) => {
          const height = window.outerHeight - (document.documentElement.offsetHeight) - 1

          element.style.height = `${height}px`
        }

        window.addEventListener('resize', debounce(() => {
          console.log('resize')
          resizeToWindowHeight(element)
        }))

        element.addEventListener('load', (e) => {
          resizeToWindowHeight(element)

          // If the iframe is still 0px height after 2 seconds,
          // it probably hasn't been able to load correctly.
          // If that happens, we set the height to 300px so the user can see it.
          setTimeout(() => {
            if (element.offsetHeight <= 0) {
              element.style.minHeight = '300px'
            }
          }, 2000)
        })
      }
    )
  }
}

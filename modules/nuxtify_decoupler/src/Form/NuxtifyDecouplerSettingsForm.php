<?php

namespace Drupal\nuxtify_decoupler\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Nuxtify Decoupler settings form.
 */
class NuxtifyDecouplerSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nuxtify_decoupler_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'nuxtify_decoupler.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nuxtify_decoupler.settings');

    $form = parent::buildForm($form, $form_state);
    $form['#tree'] = TRUE;

    $form['settings'] = [
      'frontend_url' => [
        '#type' => 'url',
        '#title' => $this->t('Frontend URL'),
        '#description' => $this->t('The URL of the frontend application.'),
        '#default_value' => $config->get('frontend_url') ?? '',
        '#required' => TRUE,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('nuxtify_decoupler.settings');
    $values = $form_state->getValues();

    foreach ($values['settings'] as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

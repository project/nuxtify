<?php

namespace Drupal\Tests\nuxtify\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests installation of the Nuxtify installation profile.
 *
 * @group nuxtify
 */
class InstallationTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'nuxtify_decoupler',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'nuxtify_admin';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'nuxtify';

  /**
   * Tests installation of the Nuxtify installation profile.
   */
  public function testInstallation() {
    $this->drupalGet('user/login');
    $this->assertSession()->statusCodeEquals(200);
  }

}
